#!/bin/bash
# What Is This?
#   A script started by a systemd simple service.
#   Scans for vbladeN_N folders (subvolumes) and for each, tests for a file of
#   the same name.  If it does not exist...
#   * Tests that there is more than 80GB free on destination volume
#   * Creates default container file of 64 GiB
#   Once the file exists...
#   * this checks for and loads loop module if not loaded
#   * Creates a loopback block device
#   * Copies template vbladeN_N.service with correct name (if it doesn't
#       exist) and starts it

$FILE=./vblade1_!

if [ -f "$FILE" ]; then
        echo "$FILE exists.  Continuing."
else
        echo "$FILE does not exist.  Creating."
        dd if=/dev/zero of=$FILE bs=1M count=524288 conv=sync,notrunc iflag=fullblock
fi