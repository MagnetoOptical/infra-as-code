import yaml


selinux_contexts = {
    "contexts": [
        {
            "path": "/var/www",
            "context": "httpd_sys_rw_content_t"
        },
        {
            "path": "*.php",
            "context": "httpd_sys_script_exec_t"
        }
    ]
}

with open('selinux_contexts.yml', 'w') as f:
    yaml.dump(selinux_contexts, f, allow_unicode=True, default_flow_style=False)