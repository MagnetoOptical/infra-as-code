# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  # User Config
  users.users.sseago = {
    isNormalUser = true;
    extraGroups = [ "wheel" "input" "audio" "video" "lp" "networkmanager" "kvm" "libvirtd" "docker"];
    shell = pkgs.bash;
  };

  # Enable X11
  services.xserver = {
    enable = true;
    # Configure Window Manager
    windowManager = {
      leftwm.enable = false;
      i3.enable = true;
    };
    # Add in XFCE4 Desktop Manager for emergencies
    desktopManager = {
      xfce.enable = true;      
    };
    # Configure Display Manager
    displayManager = {
      defaultSession = "none+i3";
      lightdm = {
        enable = true;
        greeter.enable = true;
      };
    };
  };

  # Enable printing
  services.printing.enable = true;

  # Enable Bluetooth
  services.blueman.enable = true;

  # Enable sound
  sound.enable = true;

  # Configure...
  hardware = {
    # ... Sound via Pulseaudio
    pulseaudio = {
      enable = true;
      support32Bit = true;
      extraConfig = "load-module module-switch-on-connect";
      package = pkgs.pulseaudioFull;
    };

    # ... Bluetooth
    bluetooth = {
      enable = true;
      settings = {
        General = {
          Enable = "Source,Sink,Media,Socket";
        };
      };
    };
  };

  # System Packages
  environment.systemPackages = with pkgs; [
    dex
    lsp-plugins
    ntfs3g
    python3Full
    mpd
    vorta
    xorg.xhost
    xdotool

    # Window Manager helpers
    alacritty
    feh
    conky
    dmenu
    graphicsmagick
    lemonbar
    neomutt
    nitrogen
    networkmanagerapplet
    pcmanfm
    picom
    polybar
    ranger
    slock
    xmobar
    xss-lock
    
    # User Applications
    firefox-esr
    gimp
    inkscape
  ];

  # Enable SSH for workstation
  services.openssh = {
    enable = true;
    permitRootLogin = "no";
  };

  nix = {
    settings = {
      auto-optimise-store = true;
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };
}
