# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{

  # Enable touchpad
  services.xserver.libinput.enable = true;

  # Handle closed lid when docked
  services.logind.lidSwitchDocked = "ignore";
}
